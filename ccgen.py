from reportlab.pdfgen import canvas
from reportlab.lib.units import inch
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.colors import Color, black, red, green
from reportlab.platypus import Paragraph, Frame


# global vars (constants)
w,h = (8*inch,8.5*inch)
offset = 8
fs = 2*offset # font size
sp = u'\u2660'
he = u'\u2661'
di = u'\u2662'
cl = u'\u2663'
box = u'\u2610'
selbox = u'\u2611'
blue=Color(0,0.5,1)

# Global Canvas & Frame info
allFrames = {}
c = canvas.Canvas("test.pdf",pagesize=(w,h))  # inch = 72 points

# register fonts
pdfmetrics.registerFont(TTFont('sans', 'dejavusans.ttf'))
pdfmetrics.registerFont(TTFont('sansBd', 'dejavusans-bold.ttf'))
pdfmetrics.registerFont(TTFont('sansThin', 'dejavusanscondensed.ttf'))
pdfmetrics.registerFont(TTFont('serifThin', 'dejavuserif.ttf'))
pdfmetrics.registerFont(TTFont('mono', 'Consola.ttf'))

def makegrid():
	# Lines are approximate at best for now, can be tweaked as needed later.
	
	c.setFillColor(black)
	
	c.setLineWidth(1)
	c.grid([0,w/2,w],[0,h])
	
	# Right Side of Card

	nameFrame = Frame(w/2,h-offset*3-2, w/2,offset*3+2, leftPadding=0, rightPadding=0, topPadding=0, bottomPadding=0, showBoundary=True)
	allFrames['names'] = nameFrame
	
	overviewFrame = Frame(w/2,h-offset*11, w/2,offset*8-2, leftPadding=0, rightPadding=0, topPadding=0, bottomPadding=0, showBoundary=True)
	allFrames['overview'] = overviewFrame
	
	pos = 11
	n = 16
	
	clubFrame = Frame(w/2,h-offset*(pos+n), w/4+offset,offset*n, leftPadding=0, rightPadding=0, topPadding=0, bottomPadding=0, showBoundary=True)
	allFrames['club'] = clubFrame
	
	diamondFrame = Frame(w*.75+offset,h-offset*(pos+n), w/4-offset,offset*n, leftPadding=0, rightPadding=0, topPadding=0, bottomPadding=0, showBoundary=True)
	allFrames['diam'] = diamondFrame
	
	pos+=n
	n=10
	
	majorFrame = Frame(w/2,h-offset*(pos+n), w/2,offset*n, leftPadding=0, rightPadding=0, topPadding=0, bottomPadding=0, showBoundary=True)
	allFrames['major'] = majorFrame
	
	pos+=n
	n=12
	
	oneNTFrame = Frame(w/2,h-offset*(pos+n), w/2,offset*n, leftPadding=0, rightPadding=0, topPadding=0, bottomPadding=0, showBoundary=True)
	allFrames['1NT'] = oneNTFrame
	
	pos+=n
	n=3
	
	twoNTFrame = Frame(w/2,h-offset*(pos+n), w/2,offset*n, leftPadding=0, rightPadding=0, topPadding=0, bottomPadding=0, showBoundary=True)
	allFrames['2NT'] = twoNTFrame
	
	pos+=n
	n=3
	
	threeNTFrame = Frame(w/2,h-offset*(pos+n), w/2,offset*n, leftPadding=0, rightPadding=0, topPadding=0, bottomPadding=0, showBoundary=True)
	allFrames['3NT'] = threeNTFrame
	
	pos+=n
	n=14 # 3.5 per bid
	
	twobidsFrame = Frame(w/2,h-offset*(pos+n), w/2,offset*n, leftPadding=0, rightPadding=0, topPadding=0, bottomPadding=0, showBoundary=True)
	allFrames['2bids'] = twobidsFrame
	
	pos+=n

	otherFrame = Frame(w/2,0, w/2,h-offset*pos, leftPadding=0, rightPadding=0, topPadding=0, bottomPadding=0, showBoundary=True)
	allFrames['other'] = otherFrame
	

	
	#  Left Side of Card

	pos = 0
	n = 9
	
	doubleFrame = Frame(0,h-offset*(pos+n), w/4,offset*n, leftPadding=0, rightPadding=0, topPadding=0, bottomPadding=0, showBoundary=True)
	allFrames['dbl'] = doubleFrame

	NTOCFrame = Frame(w/4,h-offset*(pos+n), w/4,offset*n, leftPadding=0, rightPadding=0, topPadding=0, bottomPadding=0, showBoundary=True)
	allFrames['NTOC'] = NTOCFrame
	
	pos += n
	n = 11

	OCFrame = Frame(0,h-offset*(pos+n), w/4,offset*n, leftPadding=0, rightPadding=0, topPadding=0, bottomPadding=0, showBoundary=True)
	allFrames['OC'] = OCFrame

	vNTFrame = Frame(w/4,h-offset*(pos+n), w/4,offset*n, leftPadding=0, rightPadding=0, topPadding=0, bottomPadding=0, showBoundary=True)
	allFrames['vNT'] = vNTFrame
	
	pos += n
	n = 10
	
	qbidFrame = Frame(0,h-offset*(pos+n), w/4,offset*n, leftPadding=0, rightPadding=0, topPadding=0, bottomPadding=0, showBoundary=True)
	allFrames['qbid'] = qbidFrame

	vTOdblFrame = Frame(w/4,h-offset*(pos+n), w/4,offset*n, leftPadding=0, rightPadding=0, topPadding=0, bottomPadding=0, showBoundary=True)
	allFrames['vTOdbl'] = vTOdblFrame
	
	pos += n
	n = 9
	
	vPreeFrame = Frame(0,h-offset*(pos+n), w/4,offset*n, leftPadding=0, rightPadding=0, topPadding=0, bottomPadding=0, showBoundary=True)
	allFrames['vPree'] = vPreeFrame

	preeFrame = Frame(w/4,h-offset*(pos+n), w/4,offset*n, leftPadding=0, rightPadding=0, topPadding=0, bottomPadding=0, showBoundary=True)
	allFrames['pree'] = preeFrame
	
	pos += n
	n = 7
	
	slamFrame = Frame(0,h-offset*(pos+n), w/2,offset*n, leftPadding=0, rightPadding=0, topPadding=0, bottomPadding=0, showBoundary=True)
	allFrames['slam'] = slamFrame

	pos += n
	n = 12
	
	cardingFrame = Frame(0,h-offset*(pos+n), w/4,offset*n, leftPadding=0, rightPadding=0, topPadding=0, bottomPadding=0, showBoundary=True)
	allFrames['carding'] = cardingFrame

	signalsFrame = Frame(w/4,h-offset*(pos+n), w/4,offset*n, leftPadding=0, rightPadding=0, topPadding=0, bottomPadding=0, showBoundary=True)
	allFrames['signals'] = signalsFrame
	
	pos += n
	
	leadSuitFrame = Frame(0,0, w/4,h-offset*pos, leftPadding=0, rightPadding=0, topPadding=0, bottomPadding=0, showBoundary=True)
	allFrames['leadSuit'] = leadSuitFrame

	leadNTFrame = Frame(w/4,0, w/4,h-offset*pos, leftPadding=0, rightPadding=0, topPadding=0, bottomPadding=0, showBoundary=True)
	allFrames['leadNT'] = leadNTFrame
	
	
	
	
def names():
	allFrames['names'].addFromList([Paragraph('<b>&nbsp;Name </b>'+'_'*29+'<b> ACBL# </b>'+'_'*9)]*2,c)
	
def overview():
	allFrames['overview'].addFromList([],c)

def club():
	allFrames['club'].addFromList([],c)

def diam():
	allFrames['diam'].addFromList([],c)
	
def major():
	allFrames['major'].addFromList([],c)

def oneNT():
	allFrames['1NT'].addFromList([],c)

def twoNT():
	allFrames['2NT'].addFromList([],c)
	
def threeNT():
	allFrames['3NT'].addFromList([],c)
	
def twobids():
	allFrames['2bids'].addFromList([],c)
	
def other():
	allFrames['other'].addFromList([],c)
	
def dbl():
	allFrames['dbl'].addFromList([],c)
	
def NTOC():
	allFrames['NTOC'].addFromList([],c)
	
def OC():
	allFrames['OC'].addFromList([],c)
	
def vNT():
	allFrames['vNT'].addFromList([],c)
	
def qbid():
	allFrames['qbid'].addFromList([],c)
	
def vTOdbl():
	allFrames['vTOdbl'].addFromList([],c)
	
def vPree():
	allFrames['vPree'].addFromList([],c)
	
def pree():
	allFrames['pree'].addFromList([],c)
	
def slam():
	allFrames['slam'].addFromList([],c)
	
def carding():
	allFrames['carding'].addFromList([],c)
	
def signals():
	allFrames['signals'].addFromList([],c)
	
def leadSuit():
	allFrames['leadSuit'].addFromList([],c)
	
def leadNT():
	allFrames['leadNT'].addFromList([],c)
	

#### Main 

c.setFont('sans',fs)
makegrid()
names()
overview()
club()
diam()
major()
oneNT()
twoNT()
threeNT()
twobids()
other()
dbl()
NTOC()
OC()
vNT()
qbid()
vTOdbl()
vPree()
pree()
slam()
carding()
signals()
leadSuit()
leadNT()

c.save()
